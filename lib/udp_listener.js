var dgram = require("dgram");
var socket = dgram.createSocket("udp4");
var message = "no udb_clock configured. "+
              "the light playback server will not be "+
              "listening to the rest of the system.";

module.exports = function(store, doer){
  socket.on("error", function (err) {
    console.log('socket error', err)
    console.log(message);
    socket.close();
  });

  socket.on("message", function (msg, rinfo) {
    doer(msg.toString());
  });

  socket.on("listening", function () {
    var address = socket.address();
    console.log("socket listening " + address.address + ":" + address.port);
  });
  
  store.getLatest('udp_clock', function(err, udp_clock){
    if(err || !udp_clock){
      console.log(message);
    }else{
      var address = udp_clock.split(':')[0];
      var port = udp_clock.split(':')[1];
      socket.bind(port, address, function(){
        socket.addMembership(address);
      });
    }
  })
}



