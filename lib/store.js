var nedb = require('nedb');
var config = require('config');
var _ = require('underscore');

var db = new nedb({
  filename: __dirname+'/../'+config.get('db_name')+'.db',
  autoload: true
});

getLatest = function(key, callback){
  db.find({ key: key}, function(err, docs){
    var latest = _(docs).max(function(d){ return d.created });
    callback(err, latest && latest.value)
  })
}

insert = function(key, value, callback){
  var doc = { key: key, value: value, created: new Date() };
  db.insert(doc, callback);
}

module.exports = {
  getLatest: getLatest,
  insert: insert
}