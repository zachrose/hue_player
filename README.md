# Hue Lights Playback Server

This is a player. It plays sequences of colors on Hue light bulbs. The sequences are imported in a JSON-based file format called ".zeitlight", which can be created and edited in a separate tool. There's a sample zeitlight file called `sergio.zeitlight` in the project root that you can use.

It runs as a web server. There's a GUI served up at "/" which describes and allows configuration of the many things that need to be configured for light playback. Configuration is stateful, meaning that what's set in the GUI will stick around between server restarts, hardware shutdown, etc.

This playback server also listens to a UDP multicast, the exact address and port of which can be set in the GUI. When this UDP service is running, the player will resync itself every ten seconds (including 0). Be advised, it will keep playing in the absence of UDP signals. (If you want to stop playback, stop the server process.)

Some configuration concerns this playback server itself, and can't be configured with the GUI. Instead, edit `config/default.json` in this directory. (The only interesting thing here is the port that the GUI runs on, which is 3000 unless you set it to something else.)

## Starting it up

On a Mac, open the Terminal application. Drag the `start_server.sh` file into the terminal window and hit `ENTER`. Then open a web browser to `http://localhost:3000` and begin configuration. To stop the server, go back the terminal and press `control-C`.