var set_udp_clock = function(){
  var address = prompt("UDP clock IP address or hostname:");
  var port = prompt("UDP clock port:");
  superagent
    .put('/setup/udp_clock')
    .set('Content-Type', 'text/plain')
    .send(address+':'+port)
    .end(function(err, res){
      if (res && res.ok) {
        $("#udp_clock h2 span").text(res.text);
        $("#udp_clock").addClass('ok');
        $("#udp_clock").removeClass('not_ok');
      } else {
        alert('Error: ' + res.text);
      }
    });
}

var set_hue_bridge = function(){
  var address = prompt("Hue Bridge IP address or hostname:");
  superagent
     .put('/setup/bridge')
     .set('Content-Type', 'text/plain')
     .set('Accept', 'text/plain')
     .send(address)
     .end(function(err, res){
       if (res.ok) {
         $("#hue_bridge h2 span").text(res.text);
         $("#hue_bridge").addClass('ok');
         $("#hue_bridge").removeClass('not_ok');
       } else {
         alert('Error: ' + res.text);
       }
     });
}

var post_user = function(){
  superagent
     .post('/setup/user')
     .set('Accept', 'text/plain')
     .end(function(err, res){
       if (res.ok) {
         $("#user h2 span").text(res.text);
         $("#user").addClass('ok');
         $("#user").removeClass('not_ok');
       } else {
         alert('Error: ' + res.text);
       }
     });
}

var put_user = function(){
  var user_id = prompt("Please enter a user id:");
  superagent
     .put('/setup/user')
     .set('Content-Type', 'text/plain')
     .set('Accept', 'text/plain')
     .send(user_id)
     .end(function(err, res){
       if (res.ok) {
         $("#user h2 span").text(res.text);
         $("#user").addClass('ok');
         $("#user").removeClass('not_ok');
       } else {
         alert('Error: ' + res.text);
       }
     });
}

var discover_bulbs = function(){
  superagent
    .post('/lights')
    .set('Accept', 'application/json')
    .end(function(err, res){
      if (res.ok) {
        $("#bulbs").addClass('ok');
        $("#bulbs").removeClass('not_ok');
        $("#bulbs p").text(JSON.stringify(res.body.lights))
      } else {
        alert('Error: ' + res.text);
      }
    });
}

var manual_play = function(){
  superagent
     .post('/play')
     .end(function(err, res){
       if (err) {
         alert('Error: ' + res.text);
       }
     });
}

var crash = function(){
  var msg = "This is for testing purposes. Proceed?"
  if(confirm(msg)){
    superagent
      .post('/crash')
      .end(function(){
        alert("Server should have crashed just now.");
      });
  }
}

var shutdown = function(){
  var msg = "This is for testing purposes. Proceed?"
  if(confirm(msg)){
    superagent
      .post('/shutdown')
      .end(function(){
        alert("Server should have shut down gracefully just now.");
      });
  }
}

$('section#udp_clock button').click(set_udp_clock);
$('section#hue_bridge button').click(set_hue_bridge);
$('section#user button#create').click(post_user);
$('section#user button#tell').click(put_user);
$('section#bulbs button').click(discover_bulbs);
$('button#play').click(manual_play);
$('button#crash').click(crash);
$('button#shutdown').click(shutdown);
