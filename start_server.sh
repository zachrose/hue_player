DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export NODE_CONFIG_DIR=$DIR/config
$DIR/nodejs/node $DIR/server.js