var app = require('./app');
var config = require('config');
var port = config.port || 3000;

var server = app.listen(port, function(){
  console.log(new Date().toString());
  console.log("Listening on port "+port);
})
