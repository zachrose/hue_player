process.env.NODE_ENV = 'test';
var request = require('supertest');
var app = require('../app.js');
var test_app = request(app);
var fs = require('fs');
var config = require('config');

var cleanup = function(){
  var path = __dirname+'/../'+config.get('db_name')+'.db';
  if(fs.existsSync(path)){
    fs.unlinkSync(path);
  }
}

before(function(done){
  this.timeout(10*1000);
  cleanup();
  test_app
    .put('/setup/bridge')
    .set('Content-Type', 'text/plain')
    .send('192.168.1.109')
    .end(function(){
      test_app.post('/setup/user').end(done);
    });
});

after(cleanup);

describe("POST /lights", function(){
  this.timeout(30*1000);
  it('@integration: searches, fetches and saves lights', function(done){
    test_app
      .post('/lights')
      .expect(200)
      .end(function(err, res){
        test_app
          .get('/lights')
          .expect(200)
          .end(done)
      });
  });
});