process.env.NODE_ENV = 'test';
var request = require('supertest');
var app = require('../app.js');
var test_app = request(app);
var fs = require('fs');
var config = require('config');
var sequence = require('./fixtures/sequence');

var cleanup = function(){
  var path = __dirname+'/../'+config.get('db_name')+'.db';
  if(fs.existsSync(path)){
    fs.unlinkSync(path);
  }
}

before(cleanup);
after(cleanup);

describe("POST/GET /sequence", function(){
  it('can set and get the sequence', function(done){
    test_app
      .post('/sequence')
      .send({ name: null, sequence: sequence })
      .expect(200)
      .end(function(err, res){
        test_app
          .get('/sequence')
          .expect(200, { name: null, sequence: sequence })
          .end(done);
      });
  });
  // it('can tell the server to look in the filesystem');
});