process.env.NODE_ENV = 'test';
var request = require('supertest');
var app = require('../app.js');
var test_app = request(app);
var fs = require('fs');
var config = require('config');

var cleanup = function(){
  var path = __dirname+'/../'+config.get('db_name')+'.db';
  if(fs.existsSync(path)){
    fs.unlinkSync(path);
  }
}

before(cleanup);
after(cleanup);

describe("POST /setup/user", function(){
  this.timeout(10*1000);
  it('@integration: can set up a new user', function(done){
    test_app
      .post('/setup/user')
      .expect(201)
      .end(function(err, res){
        test_app
          .get('/setup/user')
          .set('Accept', 'text/plain')
          .expect(200)
          .end(done);
      });
  })
});

describe("PUT/GET /setup/bridge", function(){
  it('can set and get the bridge address', function(done){
    test_app
      .put('/setup/bridge')
      .set('Content-Type', 'text/plain')
      .send("192.168.0.2")
      .expect(200)
      .end(function(err, res){
        test_app
          .get('/setup/bridge')
          .set('Accept', 'text/plain')
          .expect(200, "192.168.0.2")
          .end(done);
      });
  });
});