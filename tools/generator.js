_ = require('underscore');

var light_count = 17;
var duration = 1; // minutes
var tempo = 10; // ticks / minute
var tick_count = tempo * duration;
var duration_ms = duration*1000*60;
var tick_interval = duration_ms / tick_count;
debugger;

var hue = function(i){
  return 0;
  return _.random(0,6)*60;
  return ((i % 2 == 0) ? 180 : 0);
}
var saturation = function(i){
  return 100;
  return _.random(0,1)*100;
  return (i % 3)*33;
}
var luminosity = function(i){
  return 100;
  return _.random(50,100);
  return (i % 5)*20;
}
var transitiontime = function(i){
  return 0;
  return i;
}
var channel = function(i){
  return "light"+((i%light_count)+1);
}


var sequence = _.range(0, tick_count).reduce(function(memo, i){
  for(var light = 0; light < light_count; light++){
    memo.push({
      "t": i*tick_interval,
      "d": {
        "hue": hue(i),
        "saturation": saturation(i),
        "luminosity": luminosity(i),
        "transitiontime": transitiontime(i),
        //"on": true
      },
      "channel": channel(light)
    });
  }
  
  return memo;
  
}, []);

var file = {
  name: 'yay',
  sequence: sequence
}

console.log(JSON.stringify(file));