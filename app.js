var _ = require('underscore');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var config = require('config');
var http = require('http');
var async = require('async');
var multer  = require('multer');
var fs = require('fs');
var Hue = require('node-hue-api');
var Player = require('00101010').player;

app.use(bodyParser.json());
app.use(bodyParser.text());
var pub = __dirname + '/public';
app.use(express.static(pub));
app.use(multer());
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

var player = null;

var store = require(__dirname+'/lib/store');
var getLatest = store.getLatest;
var insert = store.insert;

var udp_listener = require('./lib/udp_listener')(store, function(timecode){
  var hours =   parseInt(timecode.split(':')[0], 10);
  var minutes = parseInt(timecode.split(':')[1], 10);
  var seconds = parseInt(timecode.split(':')[2], 10);
  var frame =   parseInt(timecode.split(':')[3], 10);
  var total_ms = seconds*1000+minutes*60*1000+hours*60*60*1000;
  if(seconds % 10 == 0 && frame == '01'){
    console.log('periodic resync', seconds, frame);
    play(total_ms, function(err){
      if(err){
        console.log('periodic resync failed', err)
      } else {
        console.log('periodic resync success')
      }
    })
  }
});

app.get('/', function(req, res){
  async.parallel({
    udp_clock: function(callback){ getLatest('udp_clock', callback) },
    address: function(callback){ getLatest('address', callback) },
    user: function(callback){ getLatest('user', callback) },
    sequence: function(callback){ getLatest('sequence', callback) },
    lights: function(callback){ getLatest('lights', callback) }
  }, function(error, result){
    if(error){ res.status(500).send(error); }
    else{
      var ctype = req.get('Content-Type') || '';
      if(ctype.match(/json/)){
        res.status(200).send(result);
      }else{
        res.status(200).render('status', result);
      }
    }
  });
});

var play = function(from_ms, callback){
  async.parallel({
    address: function(callback){ getLatest('address', callback) },
    user: function(callback){ getLatest('user', callback) },
    sequence: function(callback){ getLatest('sequence', callback) },
    lights: function(callback){ getLatest('lights', callback) }
  }, function(error, result){
    if(error){
      callback(error);
      return false;
    }else{
      var api = new Hue.HueApi(result.address, result.user);
      var sequence = JSON.parse(result.sequence);
      sequence = sequence.sequence || sequence;
      var event_count = sequence.length;
      var duration = _(sequence).max(function(e){ return e.t }).t/60000;
      console.log("\n\n\nPlaying sequence with "+event_count+" events over "+duration+" minutes\n\n\n");
      var lights = result.lights.lights;
      var lightIdFromName = function(name){
        return _(lights).find(function(light){
          return (light.name == name);
        });
      }
      var doer = function(d, channel){
        var light = lightIdFromName(channel);
        var id = light.id;
        var hue = Math.round((d.hue/360)*65535);
        var saturation = Math.min(Math.round((d.saturation/100)*255), 255);
        var brightness = Math.min(Math.round((d.luminosity/100)*255), 255);
        var transitiontime = parseInt(d.transitiontime, 10) || 0;
        api.setLightState(id, {
            "hue": hue,
            "sat": saturation,
            "bri": brightness,
            "on": !(d.luminosity == 0),
            "transitiontime": transitiontime
          })
          .then(function(){
            console.log("light "+id+" set")
          })
          .fail(function(error){
            console.log("Light "+channel+" not set", error);
          })
      }
      player = player || new Player(doer);
      player.stop().load(sequence).jumpTo(from_ms).play();
      callback();
    }
  });
}

app.post('/play', function(req, res){
  var from_ms = req.get('from_ms') || 0;
  console.log('playing from '+from_ms);
  play(from_ms, function(err){
    if(err) res.status(500).send(error);
    res.status(200)
  });
});

app.post('/lights', function(req, res){
  getLatest('address', function(err, address){
    if(err || !address){
      console.warn('error fetching latest bridge address');
      res.status(500).send('error fetching latest bridge address');
    }else{
      console.log('using address', address);
      getLatest('user', function(err, username){
        if(err || !username){
          console.warn('error fetching latest user', err, username);
          res.status(500).send(err)
        }else{
          console.log('using username', username);
          var hu = new Hue.HueApi(address, username);
          async.timesSeries(5, function(n, next){
            console.log('searching for new lights ('+n+')')
            hu.searchForNewLights(next)
          }, function(error){
            if(error){
              console.log('problem searching for new lights', error);
              res.status(500).send(error)
            }else{
              hu.lights()
                .then(function(lights){
                  insert('lights', lights, function(error, result){
                    if(error){
                      console.log('problem saving lights')
                      res.status(500).send(error);
                    }
                    else res.status(200).send(lights);
                  })
                })
                .fail(function(error){
                  console.log('problem getting the lights', error);
                  res.status(500).send(error);
                })
            }
          });
        }
      });
    }
  });
});

app.get('/lights', function(req, res){
  getLatest('lights', function(err, lights){
    if(err) res.status(500).send(err);
    else res.status(200).send(lights);
  });
});

app.post('/setup/user', function(req, res){
  getLatest('address', function(err, address){
    
    var hu = new Hue.HueApi();
    hu.registerUser(address, null, config.get('username'))
      .then(function(result){
        insert('user', result, function(err, doc){
          if(err){
            res.status(500).send("Could not save username.")
          }else{
            res.status(201).send(result);
          }
        });
      })
      .fail(function(error){
        var troubleshooting =
          "Could not connect to the bridge.\n"+
          "Press its blue button and try again.\n"+
          "If that doesn't work, are you sure the controller "+
          "and the bridge are on the same network?\n"+
          "If so, try setting up the bridge again.";
        console.info(error);
        res.status(502).send(troubleshooting);
      })
      .done(function(){
        console.log('done!');
      });
  });
});

app.put('/setup/user', function(req, res){
  var user_id = req.body;
  console.log('user_id', user_id);
  insert('user', user_id, function(err, newDoc){
    if(err) res.status(500).end();
    res.status(200).end(user_id);
  })
});


app.get('/setup/user', function(req, res){
  getLatest('user', function(err, username){
    if(err) res.status(500).send(err);
    else res.status(200).send(username);
  })
})

app.put('/setup/bridge', function(req, res){
  var address = req.body;
  console.log('address', address);
  insert('address', address, function(err, newDoc){
    if(err) res.status(500).end();
    res.status(200).end(address);
  })
});

app.get('/setup/bridge', function(req, res){
  getLatest('address', function(err, address){
    if(err) res.status(500).send(err);
    else {
      res.status(200).send(address);
    }
  })
});

app.put('/setup/udp_clock', function(req, res){
  var address = req.body;
  console.log('address', address);
  insert('udp_clock', address, function(err, newDoc){
    if(err) res.status(500).end();
    res.status(200).end(address);
  })
});

app.get('/setup/udp_clock', function(req, res){
  getLatest('udp_clock', function(err, udp_clock){
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(udp_clock);
    }
  })
});

// should be PUT, but we want to upload a file on the web in 2014
app.post('/sequence', function(req, res){
  fs.readFile(req.files.sequence.path, { encoding: 'utf8' }, function (err, sequence) {
    // HI-C SAVANT DRAGONS
    if(!sequence){
      res.status(400).send("Please upload a .zeitlight file");
    } else if(err){
      res.status(500).send("Something went wrong in working with a .zeitlight file");
    }else{
      insert('sequence', sequence, function(err, newDoc){
        if(err){
          res.status(500).send("Something went wrong saving the sequence");
        } else {
          res.status(200).render('sequence_saved');
        }
      })
    }
  });
});

app.get('/sequence', function(req, res){
  getLatest('sequence', function(err, sequence_with_name){
    if(err) res.status(500).send(err);
    else res.status(200).send(sequence_with_name);
  })
});

app.post('/crash', function(){
  console.log(new Date().toString());
  console.log("Process crashing due to deliberate user request");
  process.exit(1);
});

app.post('/shutdown', function(){
  console.log(new Date().toString());
  console.log("Process shutting down gracefully due to user request");
  process.exit(0);
});

module.exports = app;
